var builder = require("botbuilder");

findBestMatch = (entities, types) => {
    return types
        .map(element => builder.EntityRecognizer.findEntity(entities, element))
        .filter(element => element)
        .sort((a, b) => a.score - b.score)[0];
};

module.exports = { findBestMatch };
