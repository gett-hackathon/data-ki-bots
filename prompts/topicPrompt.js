var app = require("../app");
var builder = require("botbuilder");

var recognizer;

const dialog = new builder.Prompt({
    defaultRetryPrompt: "Sorry, but I didn't understand you!"
}).onRecognize((context, callback) => {
    recognizer.recognize(context, (err, result) => {
        var topic = builder.EntityRecognizer.findEntity(
            result.entities,
            "topic"
        );

        if (!topic) callback(null, 0.5, context.message.text);
        else {
            callback(null, 1, topic.entity);
        }
    });
});

const prompt = (session, prompt, options) => {
    var args = options || {};
    args.prompt = prompt || options.prompt;
    session.beginDialog("topicPrompt", args);
};

const setup = (bot, rec) => {
    bot.dialog("topicPrompt", dialog);
    recognizer = rec;
};

module.exports = { prompt, dialog, setup };
