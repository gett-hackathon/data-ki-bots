var builder = require("botbuilder");
var answers = require("../speech/answers");
var store = require("../store/suggestion");

module.exports = [
    (session, args, next) => {
        if (!session.userData.name) {
            const msg = new builder.Message(session).addAttachment(
                welcomeCard(session)
            );
            session.send(msg);
            setTimeout(() => next(), 800);
        } else {
            if (
                store.getRecentTopics(6, session.userData) &&
                store.getRecentTopics(6, session.userData).length >= 0
            )
                session.replaceDialog("recent");
            else {
                const msg = new builder.Message(session).addAttachment(
                    welcomeCard(session)
                );
                session.send(msg);
                session.send("What do you wanna learn about today?");
                session.endDialog();
            }
        }
    },
    (session, results, next) => {
        builder.Prompts.text(
            session,
            answers.formatedRandomAnswer("name.question", [])
        );
    },
    (session, results, next) => {
        session.send(
            answers.formatedRandomAnswer("name.update", [results.response])
        );

        session.userData.name = results.response;
        session.send(answers.formatedRandomAnswer("intro", []));
        session.endDialog();
    }
];

const welcomeCard = session => {
    return new builder.HeroCard(session)
        .title("Hi, I am C3Pbot")
        .text(
            "I am your personal learning advisor. Just tell me what you need to learn and I wil find the perfect learning resources for you \n\n P.S. You can also set reminders and learning goals."
        )
        .images([
            builder.CardImage.create(
                session,
                "https://gitlab.com/gett-hackathon/data-ki-bots/raw/master/c3pbot_logo.jpg"
            )
        ]);
};
