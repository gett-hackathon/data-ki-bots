var client = require("../backend/client");
var answers = require("../speech/answers");

module.exports = [
    function(session, args, next) {
        if (!session.currentArticle) {
            session.send(answers.formatedRandomAnswer("continue", []));
        }

        session.replaceDialog(
            "suggestions",
            client.querySuggestions(
                session.userData.currentArticle.topic,
                100,
                "high"
            )
        );
        session.endDialog();
    }
];
