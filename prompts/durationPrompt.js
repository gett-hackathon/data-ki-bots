var app = require("../app");
var builder = require("botbuilder");

var recognizer;

const dialog = new builder.Prompt({
    defaultRetryPrompt:
        "Sorry, but I didn't understand you! Try entering something like '30 minutes'."
}).onRecognize((context, callback) => {
    recognizer.recognize(context, (err, result) => {
        var duration = builder.EntityRecognizer.findEntity(
            result.entities,
            "builtin.datetimeV2.duration"
        );

        if (!duration || !duration.resolution.values[0].value)
            callback(null, 0.0);
        else {
            callback(null, 1, duration.resolution.values[0].value);
        }
    });
});

const prompt = (session, prompt, options) => {
    var args = options || {};
    args.prompt = prompt || options.prompt;
    session.beginDialog("durationPrompt", args);
};

const setup = (bot, rec) => {
    bot.dialog("durationPrompt", dialog);
    recognizer = rec;
};

module.exports = { prompt, dialog, setup };
