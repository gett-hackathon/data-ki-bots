var answers = require("../speech/answers");

module.exports = [
    (session, args, next) => {
        session.send(
            answers.formatedRandomAnswer("greet.greetings", [
                session.userData.name
            ])
        );

        session.endDialog();
    }
];
