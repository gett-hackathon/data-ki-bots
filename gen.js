const request = require("sync-request");
const cheerio = require("cheerio");
var fs = require("fs");

const randomTopic = () => {
    let res = request("GET", "https://www.randomlists.com/topics/things.json");
    const $ = cheerio.load(res.getBody("utf8"));
    return $(".crux")[0].children[0].data;
};

const getSyns = base => {
    let res = request(
        "GET",
        "https://www.powerthesaurus.org/" + base + "/synonyms"
    );
    const $ = cheerio.load(res.getBody("utf8"));
    let syns = [];
    const terms = $(".link--term");
    terms.map(element => {
        syns.push(terms[element].children[0].data);
    });
    return syns;
};

let out = "";

let syns = {};
syns["please"] = getSyns("please");
syns["continue"] = getSyns("continue");

for (let index = 0; index < 25; index++) {
    out += syns["please"][index] + " " + syns["continue"][index] + "\n";
}

fs.writeFile("/home/tammo/out.txt", out, function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
