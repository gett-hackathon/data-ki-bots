var builder = require("botbuilder");
var GSearch = require("../utils/gsearch");
var answers = require("../speech/answers");
var store = require("../store/suggestion");

module.exports = [
    (session, args, next) => {
        session.send(
            answers.formatedRandomAnswer("recent.loading", [
                session.userData.name
            ])
        );
        setTimeout(() => next(), 500);
    },
    session => {
        const topics = store.getRecentTopics(6, session.userData);
        const msg = joinCard(session, topics, GSearch.getImgs(topics));
        session.send(msg);
        session.endDialog();
    }
];

const joinCard = (session, topics, imgUrls) => {
    console.log(imgUrls);
    let topicCards = [];
    topics.forEach((element, index) => {
        topicCards.push(topicCard(session, element, imgUrls[index]));
    });

    return new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel)
        .attachments(topicCards);
};

const topicCard = (session, topic, img) => {
    return new builder.HeroCard(session)
        .title(topic)
        .images([builder.CardImage.create(session, img)])
        .buttons([
            builder.CardAction.postBack(
                session,
                "suggestions:[" + topic + "]",
                "Learn More"
            )
        ]);
};
