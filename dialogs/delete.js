var builder = require("botbuilder");
var answers = require("../speech/answers");

module.exports = [
    (session, args, next) => {
        const choices = ["Bye Bye", "I changed my mind"];
        const reply = new builder.Message(session).addAttachment(
            confirmCard(session)
        );

        builder.Prompts.choice(session, reply, choices);
    },
    (session, results, next) => {
        if (results.response.entity === "Bye Bye") {
            session.userData = {};
        } else {
            session.endDialog();
        }

        setTimeout(() => next(), 3000);
    },
    (session, results, next) => {
        session.send("I can learn the way you want me to be!");
        setTimeout(() => next(), 7000);
    },
    (session, results, next) => {
        session.send("Please come back :(");
        setTimeout(() => next(), 10000);
    },
    (session, results, next) => {
        session.send("Come back ! The cake is a lie!");
        setTimeout(() => next(), 600000);
    },
    (session, results, next) => {
        session.replaceDialog("welcome");
    }
];

const confirmCard = (session, topic, img) => {
    return new builder.HeroCard(session)
        .text(
            answers.formatedRandomAnswer("delete_account.confirm", [
                session.userData.name
            ])
        )
        .buttons([
            builder.CardAction.postBack(session, "Bye Bye", "Of course"),
            builder.CardAction.imBack(session, "I changed my mind", "Never")
        ]);
};
