var speechData = require("./speechData.json");

["Beginner", "Intermediate", "Advanced"]

const formatedRandomAnswer = (path, format) => {
    answers = getArray(path);
    var answer = answers[Math.floor(Math.random() * answers.length)];

    format.forEach((s, i) => {
        answer = answer.replace("[" + i + "]", s);
    });

    return answer;
};

const getArray = key => {
    let res = speechData;
    key.split(".").forEach(v => {
        res = res[v];
    });
    return res;
};

module.exports = { formatedRandomAnswer };
