const request = require("sync-request");
const cheerio = require("cheerio");

const getImgs = imgTopics => {
    let imgUrls = [];

    imgTopics.forEach(element => {
        let res = request(
            "GET",
            `https://www.google.com/search?tbs=iar%3As&tbm=isch&sa=1&q=${escape(
                element
            )}`
        );

        const $ = cheerio.load(res.getBody("utf8"));
        imgUrls.push($("img").attr("src"));
    });
    return imgUrls;
};

const getImg = imgTopic => {
    let res = request(
        "GET",
        `https://www.google.com/search?tbs=iar%3As&tbm=isch&sa=1&q=${escape(
            imgTopic
        )}`
    );

    const $ = cheerio.load(res.getBody("utf8"));
    return $("img").attr("src");
};

module.exports = { getImgs, getImg };
